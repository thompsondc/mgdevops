package com.model.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIData;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.HibernateUtil;
import com.model.EoCrsdLanguage;

@ViewScoped
@ManagedBean
@SuppressWarnings("rawtypes")
public class AdnLanguages extends PageCodeBase {
	
	private int language_ID;

	private String language_in_English;

	private String language_in_Local;
	
	private String ActionLabel = "View List";
	
	private String hiddenId;
	
	private UIData increVal = new UIData();
	
	public String getActionLabel() {
		return ActionLabel;
	}

	public void setActionLabel(String actionLabel) {
		ActionLabel = actionLabel;
	}

	public int getLanguage_ID() {
		return language_ID;
	}

	public void setLanguage_ID(int language_ID) {
		this.language_ID = language_ID;
	}

	public String getLanguage_in_English() {
		return language_in_English;
	}

	public void setLanguage_in_English(String language_in_English) {
		this.language_in_English = language_in_English;
	}

	public String getLanguage_in_Local() {
		return language_in_Local;
	}

	public void setLanguage_in_Local(String language_in_Local) {
		this.language_in_Local = language_in_Local;
	}

	public String getHiddenId() {
		return hiddenId;
	}

	public void setHiddenId(String hiddenId) {
		this.hiddenId = hiddenId;
	}

	public UIData getIncreVal() {
		return increVal;
	}

	public void setIncreVal(UIData increVal) {
		this.increVal = increVal;
	}
	
	public String doAddEnableAction() {
		try {
			setActionLabel("Add");
		} catch (Exception e) {
			// TODO: handle exception
			printAllError("Invalid input(s)");
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return "";
	}
	
	public String doEditEnableAction() {
		try {
			setActionLabel("Update");
		} catch (Exception e) {
			// TODO: handle exception
			printAllError("Invalid input(s)");
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return "";
	}
	
	public String doSearchAction() {
		try {
			setActionLabel("Search");
			
		} catch (Exception e) {
			// TODO: handle exception
			printAllError("Invalid input(s)");
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return "";
	}

	public String doAddButtonAction() {
		try {
			
			Session session = HibernateUtil.currentSession();
			Transaction tx = session.beginTransaction();
			
			EoCrsdLanguage dd = new EoCrsdLanguage();
			
			dd.setLanguage_in_English(getLanguage_in_English());
			dd.setLanguage_in_Local(getLanguage_in_Local());
			
			session.save(dd);
			tx.commit();
			
			getDbLanguageList();
			
			printAllInfo("Language added successfully!");
			System.out.println("successfully inserted!");
		} catch (Exception e) {
			// TODO: handle exception
			printAllError("Invalid input(s)");
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public String rowEditAction(){
		try {
			System.out.println("rowEditAction");
			
			try {
				
				setActionLabel("View");
				
				System.out.println("111 "+getRequestParam().get("paramId"));
				System.out.println("111 "+getRequestParam().get("language_in_English"));
				
				int paramId = Integer.parseInt(getRequestParam().get("paramId").toString());
				String paramIdStr = getRequestParam().get("paramId").toString();
				System.out.println("languageId#:: "+paramId);
				getSessionScope().put("hiddenId", paramIdStr);
				
				EoCrsdLanguage dd = getDbLanguagesUsingId(paramId);
				
				setLanguage_in_English(dd.getLanguage_in_English());
				setLanguage_in_Local(dd.getLanguage_in_Local());
				setLanguage_ID(paramId);
				setHiddenId(paramIdStr);
				
			} catch (Exception e) {
				// TODO: handle exception
				logException(e);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		}
		return "";
	}
	
	public String doBackButtonAction(){
		try {
			setActionLabel("View List");
			System.out.println("backButtonAction");
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		}
		return "";
	}
	
	public String doUpdateButtonAction() {
		try {
			
			System.out.println("Entereed.....");
			Session session = HibernateUtil.currentSession();
			Transaction tx = session.beginTransaction();
			
			System.out.println("session hiddenId ...................... "+getSessionScope().get("hiddenId").toString());
			System.out.println("getLanguageIdHidden......;: "+getHiddenId());
			EoCrsdLanguage dd = (EoCrsdLanguage) session.load(EoCrsdLanguage.class, Integer.valueOf(getSessionScope().get("hiddenId").toString()));
			
			System.out.println("getLanguage_in_English" + dd.getLanguage_in_English() );
			
			dd.setLanguage_in_English(getLanguage_in_English());
			dd.setLanguage_in_Local(getLanguage_in_Local());
			
			session.save(dd);
			tx.commit();
			
			getDbLanguageList();
			
			printAllInfo("Language details are modified successfully!");
			System.out.println("successfully updated!");
			
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return "";
	}
	
	public String doDeleteButtonAction() {
		try {
			
			Session session = HibernateUtil.currentSession();
			Transaction tx = session.beginTransaction();
			
			EoCrsdLanguage dd = (EoCrsdLanguage) session.load(EoCrsdLanguage.class, Integer.valueOf(getSessionScope().get("hiddenId").toString()));
			
			System.out.println("getLanguage_in_English" + dd.getLanguage_in_English() );
			
			setActionLabel("View List");
			
			session.delete(dd);
			tx.commit();
			
			getDbLanguageList();
			
			printAllInfo("Language deleted successfully!");
			System.out.println("successfully deleted!");
			
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return "";
	}
	
	public List<EoCrsdLanguage> getDbLanguageList(){
		Session session = HibernateUtil.currentSession();
		List<EoCrsdLanguage> ddList = new ArrayList<EoCrsdLanguage>();
		String SQL_QUERY = "FROM EoCrsdLanguage order by language_in_English asc";
		Query query = session.createQuery(SQL_QUERY);
		try {
			Transaction transaction = session.beginTransaction();
		 	for(Iterator it=query.iterate();it.hasNext();){
		 		EoCrsdLanguage dd =(EoCrsdLanguage)it.next();
		 		System.out.println("getLanguage_in_English: " + dd.getLanguage_in_English());
		 		ddList.add(dd);
		 	}
		 	transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
	 	return ddList;
	}
	
	public EoCrsdLanguage getDbLanguagesUsingId(int id){
		EoCrsdLanguage dd = null;
		try {
			Session session = HibernateUtil.currentSession();
			String SQL_QUERY ="from EoCrsdLanguage where language_ID = "+ id;
		 	Query query = session.createQuery(SQL_QUERY);
		 	for(Iterator it=query.iterate();it.hasNext();){
		 		dd = (EoCrsdLanguage)it.next();
		 		System.out.println("PASSING USER ID getLanguage_ID: " + dd.getLanguage_ID());
		 		System.out.println("PASSING USER ID getLanguage_in_English: " + dd.getLanguage_in_English());
		 		System.out.println("PASSING USER ID getLanguage_in_Local: " + dd.getLanguage_in_Local());
		 	}
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		} finally {
			HibernateUtil.closeSession();
		}
		return dd;
	}
	
}
