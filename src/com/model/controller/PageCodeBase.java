package com.model.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.em.general.ProductConstants;

import com.sun.faces.context.FacesContextImpl;

@SuppressWarnings({"rawtypes"})
public abstract class PageCodeBase extends ProductConstants {
	
	public PageCodeBase() {
	
	}
	
	public static UIComponent findComponent(UIComponent base, String id) {

		// Is the "base" component itself the match we are looking for?
		if (id.equals(base.getId())) {
			return base;
		}

		// Search through our facets and children
		UIComponent kid = null;
		UIComponent result = null;
		Iterator kids = base.getFacetsAndChildren();
		while (kids.hasNext() && (result == null)) {
			kid = (UIComponent) kids.next();
			if (id.equals(kid.getId())) {
				result = kid;
				break;
			}
			result = findComponent(kid, id);
			if (result != null) {
				break;
			}
		}
		return result;
	}

	public static UIComponent findComponentInRoot(String id) {
		UIComponent ret = null;

		FacesContext context = FacesContextImpl.getCurrentInstance();
		if (context != null) {
			UIComponent root = context.getViewRoot();
			ret = findComponent(root, id);
		}

		return ret;
	}
	
	public String url;
	
	public Map getApplicationScope() {
		return getFacesContext().getExternalContext().getApplicationMap();
	}

	public FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public Map getRequestParam() {
		return getFacesContext().getExternalContext().getRequestParameterMap();
	}

	public Map getRequestScope() {
		return getFacesContext().getExternalContext().getRequestMap();
	}

	public Map getSessionScope() {
		return getFacesContext().getExternalContext().getSessionMap();
	}
	
	public void printFieldError(String fieldId, String eMsg){
		getFacesContext().addMessage(fieldId, 
		          new FacesMessage(FacesMessage.SEVERITY_ERROR, eMsg, null));
	}
	
	public void printFieldWarning(String fieldId, String wMsg){
		getFacesContext().addMessage(fieldId, 
		          new FacesMessage(FacesMessage.SEVERITY_WARN, wMsg, null));
	}
	
	public void printFieldInfo(String fieldId, String iMsg){
		getFacesContext().addMessage(fieldId, 
		          new FacesMessage(FacesMessage.SEVERITY_INFO, iMsg, null));
	}
	
	public void printAllError(String eMsg){
		getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, eMsg, null));
	}
	
	public void printAllWarning(String wMsg){
		getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, wMsg, null));
	}
	
	public void printAllInfo(String iMsg){
		getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, iMsg, null));
	}
	
	protected void logException(Throwable throwable) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		throwable.printStackTrace(printWriter);
		log(stringWriter.toString());
	}
	
	protected void log(String message) {
		System.out.println(message);
	}
	
	/*public String getStorageRootPath() {
		AdnStorage as = new AdnStorage();
		return ds.getStoragePath();
	  }*/
	
    public static String getFileNameFromPart(Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : partHeader.split(";")) {
            if (content.trim().startsWith("filename")) {
                String fileName = content.substring(content.indexOf('=') + 1)
                        .trim().replace("\"", "");
                return fileName;
            }
        }
        return null;
    }
	
	public boolean isInteger(String input) {
		try {
			if(!input.equals("")) {
				Integer.parseInt(input);  
				return true;
				
			} else {
				return true;
			}
		
	   } catch(Exception e) {
	      return false;  
	   }
	}
	
	public boolean isBoolean(String input) {
		try {
			input = input.toLowerCase().trim();
			
			if(input.equals(""))
				return false;
			
			if(input.equals("n") || input.equals("y") || input.equals("true") || input.equals("false") || input.equals("no") || input.equals("yes"))
				return true;
			else
				return false;
			
		} catch(Exception e) {
			return false;  
		}
	}
	
	public boolean isDate(String input) {
		boolean valid = false;
		try {
			if(!input.equals("")) {
				if(input.length()==10) {
					if(input.substring(0, 3).contains("/") && input.substring(3, 6).contains("/") ) {
						int yearPos = input.lastIndexOf("/");
						int day, month, year, daysInMonth = 0;
						String leap, monthValid, dayValid, yearValid = "no";
			    
						year = Integer.parseInt(input.substring(yearPos+1));
						month = Integer.parseInt(input.substring(3,5));
						day = Integer.parseInt(input.substring(0,2));
						
						if(year >= 0000) {
							yearValid = "yes"; 
						} else {
							yearValid = "no"; 
						}
						
						if (year % 400 == 0) {
							leap = "yes"; 
						} else if (year % 100 == 0) {
							leap = "no"; 
						} else if (year % 4 == 0) {
							leap = "yes"; 
						} else {
							leap = "no"; 
						}
				    	
						switch (month) {
							case 1: daysInMonth = 31; 
							break; 
							
							case 3: daysInMonth = 31; 
							break; 
							
							case 4: daysInMonth = 30; 
							break; 
							
							case 5: daysInMonth = 31;
							break; 
							
							case 6: daysInMonth = 30; 
							break; 
							
							case 7: daysInMonth = 31; 
							break; 
							
							case 8: daysInMonth = 31; 
							break; 
							
							case 9: daysInMonth = 30; 
							break; 
							
							case 10: daysInMonth = 31; 
							break; 
							
							case 11: daysInMonth = 30; 
							break; 
							
							case 12: daysInMonth = 31; 
							break;                    
						}
						
						if (month >=1 && month <=12) {
							monthValid = "yes"; 
						} else {
							monthValid = "no"; 
						}
						
						if (month == 2) {
							if(leap == "no") {
								daysInMonth = 28; 
							} else if (leap == "yes") {
								daysInMonth = 29; 
							}
						}
						
						if (1 <= day && day <= daysInMonth ) {
							dayValid = "yes"; 
			        	} else {
			        		dayValid = "no"; 
				        }
						
						if ((dayValid == "yes") && (monthValid == "yes") && (yearValid == "yes")) { 
							valid = true;
				        } else {
				        	valid = false;
				        }
					}
				}
				
			} else {
				valid = false;
			}
			return valid;
			
		} catch(Exception e) {
			return false;  
		}
	}
	
	// validate mail id
	public boolean isEmailId(String emailId) {
		boolean flag = false;
		if(emailId.contains("@") && emailId.contains(".")) {
			int atPosition = emailId.indexOf("@");
			String subStringBeforeAt = emailId.substring(0,atPosition);
			String subStringAfterAt = emailId.substring(atPosition+1);
			if(subStringBeforeAt.matches("[a-zA-Z_.0-9]*")) {
				if(subStringAfterAt.matches("[a-zA-Z.]*"))
					flag = true;
				else
					flag = false;
			} else {
				flag = false;
			}	
		} else {
			flag = false;
		}
		return flag;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		try {
			url = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRequestURL().toString();
			url = url.substring(0, url.lastIndexOf("/")+1);
			return url;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";
	}
	
	public static boolean isInternetReachable() {
        try {
            //make a URL to a known source
            URL url = new URL("http://www.google.com");

            //open a connection to that source
            HttpURLConnection urlConnect = (HttpURLConnection) url.openConnection();

            //trying to retrieve data from the source. If there
            //is no connection, this line will fail
            urlConnect.getContent();

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }
	
	public String getUniWord(String plainText){
		String uniWord = ""; 
        try { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte encryptedData[] = md.digest(plainText.getBytes()); 
            StringBuffer hexString = new StringBuffer(); 
            for (int i = 0; i < encryptedData.length; i++) { 
                String hex = Integer.toHexString(0xFF & encryptedData[i]); 
                if (hex.length() == 1) { 
                    hexString.append('0'); 
                } 
                hexString.append(hex); 
            } 
            uniWord = hexString.toString(); 
  
        } catch (NoSuchAlgorithmException e) { 
            e.printStackTrace(); 
        }
        return uniWord;
	}
	
	Cipher ecipher;
    Cipher dcipher;
    // 8-byte Salt
    byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
    };
    // Iteration count
    int iterationCount = 19;
    String secretKey="ezeon8547";
    
	public String encrypt(String plainText) 
            throws NoSuchAlgorithmException, 
            InvalidKeySpecException, 
            NoSuchPaddingException, 
            InvalidKeyException,
            InvalidAlgorithmParameterException, 
            UnsupportedEncodingException, 
            IllegalBlockSizeException, 
            BadPaddingException{
		
        //Key generation for enc and desc
        KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
        SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);        
         // Prepare the parameter to the ciphers
        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

        //Enc process
        ecipher = Cipher.getInstance(key.getAlgorithm());
        ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);      
        String charSet="UTF-8";       
        byte[] in = plainText.getBytes(charSet);
        byte[] out = ecipher.doFinal(in);
        String encStr=new sun.misc.BASE64Encoder().encode(out);
        return encStr;
    }
     /**     
     * @param secretKey Key used to decrypt data
     * @param encryptedText encrypted text input to decrypt
     * @return Returns plain text after decryption
     */
    public String decrypt(String encryptedText)
     throws NoSuchAlgorithmException, 
            InvalidKeySpecException, 
            NoSuchPaddingException, 
            InvalidKeyException,
            InvalidAlgorithmParameterException, 
            UnsupportedEncodingException, 
            IllegalBlockSizeException, 
            BadPaddingException, 
            IOException{
         //Key generation for enc and desc
        KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
        SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);        
         // Prepare the parameter to the ciphers
        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
        //Decryption process; same key will be used for decr
        dcipher=Cipher.getInstance(key.getAlgorithm());
        dcipher.init(Cipher.DECRYPT_MODE, key,paramSpec);
        byte[] enc = new sun.misc.BASE64Decoder().decodeBuffer(encryptedText);
        byte[] utf8 = dcipher.doFinal(enc);
        String charSet="UTF-8";     
        String plainStr = new String(utf8, charSet);
        return plainStr;
    }
}
