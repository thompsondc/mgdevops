package com.model.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class StuMain extends PageCodeBase {
	@SuppressWarnings("unchecked")
	public String pageNavigationAction(){
		try {
			getSessionScope().put("currentPage", getRequestParam().get("pageName").toString()); 
			getSessionScope().put("menuId", getRequestParam().get("menu").toString());
			getSessionScope().put("filteredUserTypeId", Integer.parseInt(getSessionScope().get("userTypeId").toString()));
				
		} catch (Exception e) {
			// TODO: handle exception
			logException(e);
		}
		return "";
	}
}
