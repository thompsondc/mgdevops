package com.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the eo_crsd_languages database table.
 * 
 */
@Entity
@Table(name="eo_crsd_languages")
@NamedQuery(name="EoCrsdLanguage.findAll", query="SELECT e FROM EoCrsdLanguage e")
public class EoCrsdLanguage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int language_ID;

	private String language_in_English;

	private String language_in_Local;

	public EoCrsdLanguage() {
	}

	public int getLanguage_ID() {
		return this.language_ID;
	}

	public void setLanguage_ID(int language_ID) {
		this.language_ID = language_ID;
	}

	public String getLanguage_in_English() {
		return this.language_in_English;
	}

	public void setLanguage_in_English(String language_in_English) {
		this.language_in_English = language_in_English;
	}

	public String getLanguage_in_Local() {
		return this.language_in_Local;
	}

	public void setLanguage_in_Local(String language_in_Local) {
		this.language_in_Local = language_in_Local;
	}

}