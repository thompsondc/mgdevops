package com.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@SuppressWarnings({ "rawtypes", "deprecation","unchecked" })
public class HibernateUtil {  
    
    private static SessionFactory sessionFactory;  
      
    static{  
        try {  
            sessionFactory = new Configuration().configure().buildSessionFactory();  
        } catch (Throwable e) {  
        	// Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + e);
            throw new ExceptionInInitializerError(e);  
        }  
    }  
  
    public static SessionFactory getSessionFactory(){  
        return sessionFactory;  
    }  
      
    public static void shutDown(){  
        //closes caches and connections  
        getSessionFactory().close();  
    }  
    
    
	public static final ThreadLocal session = new ThreadLocal();
    
	public static Session currentSession() {
	    Session s = (Session) session.get();
	    // Open a new Session, if this Thread has none yet
	    if (s == null) {
		    s = sessionFactory.openSession();
		    session.set(s);
	    }
	    return s;
    }
    
	public static void closeSession() {
	    Session s = (Session) session.get();
	    if (s != null)
	    s.close();
	    session.set(null);
    }
}  

/*import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			Configuration configuration = new Configuration().configure();
			ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
			registry.applySettings(configuration.getProperties());
			ServiceRegistry serviceRegistry = registry.buildServiceRegistry();

			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		}

		return sessionFactory;
	}

}
*/






/*
 * ABOVE IS DEPRECATED
 * import org.hibernate.SessionFactory;
 */
/*import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			// loads configuration and mappings
			Configuration configuration = new Configuration().configure();
			ServiceRegistry serviceRegistry	= new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

			// builds a session factory from the service registry
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		}

		return sessionFactory;
    }
}*/




/*import org.hibernate.cfg.AnnotationConfiguration;
10.import org.hibernate.SessionFactory;
11. 
12.*//**
13. * Hibernate Utility class with a convenient method to get Session Factory object.
14. *
15. * @author nb
16. *//*
17.public class HibernateUtil {
18.    private static final SessionFactory sessionFactory;
19. 
20.    static {
21.        try {
22.            // Create the SessionFactory from standard (hibernate.cfg.xml)
23.            // config file.
24.            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
25.        } catch (Throwable ex) {
26.            // Log the exception.
27.            System.err.println("Initial SessionFactory creation failed." + ex);
28.            throw new ExceptionInInitializerError(ex);
29.        }
30.    }
31. 
32.    public static SessionFactory getSessionFactory() {
33.        return sessionFactory;
34.    }
35.}
*/