package org.em.general;


/**
 * @author Virudhai Paul
 */
public class ProductConstants {
	
	
	public static String    ADMIN_DESCR								= "Administrator"; // reflected in faces config file
	
	public static boolean   FULLSCREEN								= false;
	
	public static boolean   VERSION_DOCUMENT						= false;
	
	public static int	BUFFER_SIZE									= 1024;
	
}
